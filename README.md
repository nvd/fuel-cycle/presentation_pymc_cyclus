# Using Fuel Cycle Simulators and Bayesian Inference in Nuclear Archaeology
## Introduction
This repository contains code used during the preparation of the summary _Using
Fuel Cycle Simulators and Bayesian Inference in Nuclear Archaeology_ by Max
Schalz, Lewin Bormann and Malte Göttsche, a summary submitted to the ANS 2022
Annual Meeting.

## Software listing
The following table lists the software used in the case study (all free and
open-source).
Additional, not listed here, are 'common suspects', i.e., Python modules such
as `Pandas`, `NumPy`, `Matplotlib`, `Seaborn`.
For these modules, we did not need to pick a specific version.

| Name | Version | Notes |
|:-----|---:|:---|
| [bayesian-cycle](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/tree/ans2022/sampling)| [`2b511eb8`](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/tree/2b511eb82467ddaf139e62705a932badff1d1dc3/sampling) | See note below |
| [Cyclus](https://fuelcycle.org/) | [`b45efc6`](https://github.com/maxschalz/cyclus/tree/b45efc6d988c5d30895b320ded235222ce5a2053) | cherry-picked version with custom patches |
| Cycamore | [`bbfa385`](https://github.com/maxschalz/cycamore/tree/bbfa3856dc83a89a2f198ca624e040d729b0b399) | cherry-picked version with minor custom additions |
| Flexicamore | [`ba95e55e`](https://git.rwth-aachen.de/nvd/fuel-cycle/flexicamore/-/tree/ba95e55ec13c69a45238ea28c43baa811c7815eb) | [mirrored repository](https://github.com/maxschalz/flexicamore) on GitHub |
| [Arviz](https://arviz-devs.github.io/arviz/index.html) | [`e3afed56`](https://git.rwth-aachen.de/lewin/arviz/-/tree/lewin) | cherry-picked version with custom patches |
| [PyMC3](https://docs.pymc.io/en/v3/) | [`9000bf86`](https://git.rwth-aachen.de/lewin/pymc3/-/tree/lewin) | cherry-picked version with custom patches |

### `bayesian-cycle`
The commit specified in the table above is the state of the program upon
submission of the article to ANS.
Since then, the software has been developed further and it became an independent
Python package called Bicyclus.
Bicyclus can be found [on GitHub](https://github.com/Nuclear-Verification-and-Disarmament/bicyclus.git).

## License
Any code in this repository is licensed under
[BSD-3-Clause](https://spdx.org/licenses/BSD-3-Clause.html).
Documents published in this repository (including, but not limited to, articles,
PDF-files and presentations) are licensed under
[CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html), unless indicated
otherwise.
